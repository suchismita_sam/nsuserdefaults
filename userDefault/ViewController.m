//
//  ViewController.m
//  userDefault
//
//  Created by Click Labs134 on 11/17/15.
//  Copyright © 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

NSDictionary *userInfo;
NSMutableArray *usernameAndPassword;



@interface ViewController ()
@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;
@property (strong, nonatomic) IBOutlet UILabel *passwordLabel;
@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;

@end

@implementation ViewController

@synthesize usernameLabel;
@synthesize usernameTextField;
@synthesize passwordLabel;
@synthesize passwordTextField;


- (void)viewDidLoad {
    [super viewDidLoad];
    passwordTextField.secureTextEntry=YES;
    
    // Do any additional setup after loading the view, typically from a nib.
}

//segue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"goToUsernameScreen"])
    {

        usernameAndPassword=[[NSMutableArray alloc]init];
        [usernameAndPassword addObject:self.usernameTextField.text];
        [usernameAndPassword addObject:self.passwordTextField.text];
        
        [[NSUserDefaults standardUserDefaults]setObject:usernameAndPassword forKey:@"loginData"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
