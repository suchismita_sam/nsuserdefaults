//
//  ViewController2.m
//  userDefault
//
//  Created by Click Labs134 on 11/17/15.
//  Copyright © 2015 clicklabs. All rights reserved.
//

#import "ViewController2.h"

@interface ViewController2 ()
@property (strong, nonatomic) IBOutlet UIButton *showUsernameButton;

@end

@implementation ViewController2
@synthesize showUsernameButton;
@synthesize destdata1;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showUsername:(id)sender
{
    [self alert1];
}

-(void)alert1{
    NSMutableArray *Username=[[NSUserDefaults standardUserDefaults]objectForKey:@"loginData"];
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Username" message:[NSString stringWithFormat:@"%@",Username[0]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert1 show];
    ;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
